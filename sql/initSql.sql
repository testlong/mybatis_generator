create database test;

use test;

CREATE TABLE `target_info` (
	`id` BIGINT(16) NOT NULL AUTO_INCREMENT COMMENT '主键',
	`person_id` VARCHAR(20) NOT NULL COMMENT '人员ID',
	`target_month` VARCHAR(7) NOT NULL COMMENT '目标月份',
	`target_sj` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '目标sj',
	`target_sk` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '目标sk',
	`target_qy` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '目标qy',
	`is_del` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '是否删除(0:否 1:是)',
	`create_id` VARCHAR(36) NOT NULL DEFAULT '' COMMENT '创建人',
	`create_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
	`last_modify_id` VARCHAR(36) NOT NULL DEFAULT '' COMMENT '修改人',
	`last_modify_time` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
	PRIMARY KEY (`id`),
	INDEX `index_person_id` (`person_id`)
)
COMMENT='目标信息'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
